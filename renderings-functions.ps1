Function Custom-GetRendering {
    Param ($pageItem, $renderingPlaceholder, $renderingId)
    $rendering = Get-Rendering -Item $pageItem -PlaceHolder $renderingPlaceholder | Where-Object {$_.ItemID -eq $renderingId}
    Write-Output $rendering
}

Function Custom-AddRendering{
    Param ($pageItem, $renderingPlaceholder, $renderingId)
    $renderingItem = Get-Item -Path "master" -ID $renderingId | New-Rendering -Placeholder $renderingPlaceholder
    Add-Rendering -Item $pageItem -PlaceHolder $renderingPlaceholder -Instance $renderingItem
    Write-Host "New rendering added" -fore green
}

Function Custom-SetRenderingDatasource{
    Param ($pageItem, $renderingPlaceholder, $renderingId, $datasource)
    Get-Rendering -Item $pageItem -PlaceHolder $renderingPlaceholder | Where-Object {$_.ItemID -eq $renderingId} |
        Foreach-Object { 
            $_.DataSource = $datasource
            Set-Rendering -Item $pageItem -Instance $_
        }
    Write-Host "Updated rendering datasource" -fore green
}

Function Custom-SetRenderingPlaceholder{
    Param ($pageItem, $renderingPlaceholder, $renderingId, $placeholder)
    Get-Rendering -Item $pageItem -PlaceHolder $renderingPlaceholder | Where-Object {$_.ItemID -eq $renderingId} |
        Foreach-Object { 
            $_.Placeholder = $placeholder
            Set-Rendering -Item $pageItem -Instance $_
        }
    Write-Host "Updated rendering placeholder" -fore green
}

Function Custom-SetRenderingParameter{
    Param ($pageItem, $renderingPlaceholder, $renderingId, $parameters)
    Get-Rendering -Item $pageItem -PlaceHolder $renderingPlaceholder | Where-Object {$_.ItemID -eq $renderingId} |
        Set-RenderingParameter -Parameter $parameters |
        Set-Rendering -Item $pageItem
    Write-Host "Updated rendering parameter" -fore green
}

$Item = Get-Item -Path "master" -ID "{CC3EA998-72AC-45C7-8B27-23E0E1A18DA4}"
$RenderingId = "{7C987775-545C-4FB0-9258-4454F361124E}"

$RenderingPlaceholderNew = "main/new"
$RenderingPlaceholderUpdated = "main/updated"
$DataSourceNew = "{D13F0C33-C716-45B9-9AA0-83EFBD053609}"
$ParametersNew = [ordered]@{"Banner Size"="{9000587D-1F41-4CFF-B833-92F5B62F146F}"}

Custom-AddRendering $Item $RenderingPlaceholderNew $RenderingId
$Rendering = Custom-GetRendering $Item $RenderingPlaceholderNew $RenderingId
Custom-SetRenderingParameter $Item $RenderingPlaceholderNew $RenderingId $ParametersNew
Custom-SetRenderingDatasource $Item $RenderingPlaceholderNew $RenderingId $DataSourceNew
Custom-SetRenderingPlaceholder $Item $RenderingPlaceholderNew $RenderingId $RenderingPlaceholderUpdated