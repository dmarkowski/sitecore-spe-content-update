Function Custom-CreateItem {
    Param ($path, $name, $templateId)
    New-Item -Path $path -Name $name -ItemType $templateId
    Write-Host "New item create" -fore green
}

Function Custom-RemoveItem {
    Param ($itemId)
    Get-Item -Path "master" -ID $itemId | Remove-Item
    Write-Host "Item removed" -fore green
}

Function Custom-EditItem {
    Param ($item, $fieldName, $fieldValue)
    $item.Editing.BeginEdit()
    $item[$fieldName] = $fieldValue
    $item.Editing.EndEdit()
    Write-Host "Item editted" -fore green
}

$NewItemPath = "master:\content\Home"
$NewItemName = "New Content Page"
$NewItemTemplateId = "{B432D669-5B65-4E91-BB17-24645798B14B}"

Custom-CreateItem $NewItemPath $NewItemName $NewItemTemplateId
$Item = Get-Item -Path "master:\content\Home\New Content Page"
Custom-EditItem $Item "Description" "Test Description"

Custom-RemoveItem $Item.ID