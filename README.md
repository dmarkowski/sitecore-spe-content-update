# Sitecore content update automation with PowerShell Extentions (SPE)

SPE is a module that provides a command line and environment to run scripts in PowerShell query language. SPE is commonly used to automate content update, perform statistical analysis and many more. 

In the following paragraphs, I present few most common content update tasks that developers might want to automate. It can speed up the entire development process by reducing manual steps across testing, maintaining  environments and deployment.

Each task is described in format:
### \<Title\>
\<Story description\>
```PowerShell
<Script with solution>
```
## Working with items

### Create item
I want to create an item called *New Content Page*, based on a given template and placed under the item *\content\Home*.
```PowerShell
$newItemPath = "master:\content\Home"
$newItemName = "New Content Page"
$newItemTemplateId = "{D181D669-5B65-4E91-BB17-24645798B14B}" # new item template id

New-Item -Path $newItemPath -Name $newItemName -ItemType $newItemTemplateId
```

### Modify item
I want to set a values of the field *Description* in two items. The first item is identified by its path *\content\Home\First Item* and the second one by its ID *{B341D669-5B65-4E91-BB17-24645798A123}*. 
```PowerShell
$firstItemPath = "master:\content\Home\First Item"
$secondItemId = "{B341D669-5B65-4E91-BB17-24645798A123}"
$fieldName = "Description"

Function Custom-EditItem {
    Param ($item, $fieldName, $fieldValue)
    $item.Editing.BeginEdit()
    $item[$fieldName] = $fieldValue
    $item.Editing.EndEdit()
}

$firstItem = Get-Item -Path $firstItemPath # get item by path
$secondItem = Get-Item -Path "master" -ID $secondItemId # get item by ID

Custom-EditItem $firstItem $fieldName "First item test description"
Custom-EditItem $secondItem $fieldName "Second item test description"
```

### Delete item
"The item *\content\Home\Old Item* is obsolite, so I want to delete it."
```PowerShell
$oldItemPath = "master:\content\Home\Old Item"

Get-Item -Path $oldItemPath | Remove-Item
```

## Working with renderings

### Add rendering
I want to add a new rendering to the item *content\Home\Content Page*. The rendering is described by its ID *{6C587775-545C-4FB0-9258-4454F361124E}*.
```PowerShell
$pageItem = Get-Item -Path "master:\content\Home\Content Page"
$renderingId = "{6C587775-545C-4FB0-9258-4454F361124E}"
$renderingPlaceholder = "main"

$renderingItem = Get-Item -Path "master" -ID $renderingId | New-Rendering -Placeholder $renderingPlaceholder
Add-Rendering -Item $pageItem -PlaceHolder $renderingPlaceholder -Instance $renderingItem
```

### Set rendering datasource
I want to set a datasource for rendering. The rendering is described by its ID *{6C587775-545C-4FB0-9258-4454F361124E}* and placeholder *main*. The datasource item is described by its ID *{A6350C33-C716-45B9-9AA0-83EFBD053609}*.
```PowerShell
$pageItem = Get-Item -Path "master:\content\Home\Content Page"
$renderingId = "{6C587775-545C-4FB0-9258-4454F361124E}"
$renderingPlaceholder = "main"
$datasourceId = "{A6350C33-C716-45B9-9AA0-83EFBD053609}"

Get-Rendering -Item $pageItem -PlaceHolder $renderingPlaceholder | Where-Object {$_.ItemID -eq $renderingId} |
        Foreach-Object { 
            $_.DataSource = $datasourceId
            Set-Rendering -Item $pageItem -Instance $_
        }
```
### Set rendering parameter
I want to set a value of the rendering parameter *Display Subtitle* to true. The rendering is descibed by its ID *{6C587775-545C-4FB0-9258-4454F361124E}* and placeholder *main*. The rendering parameter *Display Subtitle* is a checkbox.
````PowerShell
$parameters = [ordered]@{"Display Subtitle"="1"}
$pageItem = Get-Item -Path "master:\content\Home\Content Page"
$renderingId = "{6C587775-545C-4FB0-9258-4454F361124E}"
$renderingPlaceholder = "main"

Get-Rendering -Item $pageItem -PlaceHolder $renderingPlaceholder | Where-Object {$_.ItemID -eq $renderingId} |
        Set-RenderingParameter -Parameter $parameters |
        Set-Rendering -Item $pageItem
````
### Change rendering placeholder
I want to change a placeholder of the rendering from *main* to *footer*. The rendering is identified by its ID *{6C587775-545C-4FB0-9258-4454F361124E}* and placeholder *main*.
````PowerShell
$pageItem = Get-Item -Path "master:\content\Home\Content Page"
$renderingId = "{6C587775-545C-4FB0-9258-4454F361124E}"
$renderingPlaceholder = "main"
$newRenderingPlaceholder = "footer"

Get-Rendering -Item $pageItem -PlaceHolder $renderingPlaceholder | Where-Object {$_.ItemID -eq $renderingId} |
        Foreach-Object { 
            $_.Placeholder = $newRenderingPlaceholder
            Set-Rendering -Item $pageItem -Instance $_
        }
````    
## Working with packages
### Install package
I want to install the package specified by its name.
````PowerShell
$packageName = "PackageWithContent"

Install-Package -Path $packageName -InstallMode Overwrite
````    


## Useful links:
* [SPE documentation](https://doc.sitecorepowershell.com)
* [SPE repository](https://github.com/SitecorePowerShell/Console)
* [SPE in Sitecore marketplace](https://marketplace.sitecore.net/en/Modules/Sitecore_PowerShell_console.aspx)